class Player:
    def __init__(self, name, health, list_of_weapons):
        self.name = name
        self.health = health
        self.list_of_weapons = list_of_weapons
        
    def shoot(self, current_weapon,enemy_player):
        current_damage = current_weapon.fire()
        enemy_player.take_damage(current_damage)
    def reload(self, current_weapon):
        current_weapon.reload()
    def take_damage(self, damage):
        self.set_health -= current_damage
    def get_name(self):
        return self._name
    def set_name(self, new_name):
        self._name = new_name
    def health(self, new_health):
        return self._health == new_health
    def get_health(self):
        return self._health
    def list_of_weapons(self, new_list_of_weapons):
        return self.list_of_weapons == new_list_of_weapons
class ArmoredPlayer(Player):
    def __init__(self, name, health,armor, list_of_weapons):
        super().__init.__(name, health,list_of_weapons)
        self.armor = armor
    def get_damage(self):
        pass
        
    
class Weapon:
    def __init__(self, damage, clip_size, clip_bullets):
        self.damage = damage
        self.clip_size = clip_size
        self.clip_bullets = clip_bullets
    def reload(self):
        self.clip_bullets = self.clip_size
    def fire(self):
        if self.clip_bullets >= [0]:
            self.clip_bullets -=1
        else:
            print("Please reload!")
        return 0
    
